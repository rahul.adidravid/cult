#!/bin/bash
source /home/agrex/anaconda3/bin/activate general
HUEY_PATH=$(whereis huey_consumer.py | awk '{print $2}')

if [ -z "$HUEY_PATH" ]; then
    echo "Error: huey_consumer.py not found"
    exit 1
fi

CURRENT_DIR=$(pwd)

cd $CURRENT_DIR
# Construct the full command
COMMAND="$HUEY_PATH huey_worker.huey -w 8 -k thread"

# Set PYTHONPATH to include /usr/bin/python3
export PYTHONPATH="/usr/bin/python3:$PYTHONPATH"

# Run the command
$COMMAND
