import os
import time
import yaml
import datetime
import numpy as np
import cv2
from threading import Thread
from helpers import *
from huey_worker import sendAlerts

lower_purple = np.array([116, 78, 130])
upper_purple = np.array([120, 113, 204])

def generate_heatmap(frame, purple_detected_grid):
    heatmap = np.zeros_like(frame, dtype=np.uint8)
    grid_size = purple_detected_grid.shape[0]
    height, width, _ = frame.shape
    cell_height, cell_width = height // grid_size, width // grid_size

    for i in range(grid_size):
        for j in range(grid_size):
            if purple_detected_grid[i, j]:
                color = (0, 255, 0)  # Green
                transparency = 0.5
            else:
                color = (0, 0, 255)  # Red

                transparency = 0.5

            x1, y1 = j * cell_width, i * cell_height
            x2, y2 = (j + 1) * cell_width, (i + 1) * cell_height
            cv2.rectangle(heatmap, (x1, y1), (x2, y2), color, -1)

    heatmap = cv2.addWeighted(frame, 1 - transparency, heatmap, transparency, 0)
    return heatmap

def result_process(result, img, purple_detected_grid):
    grid_size = 10
    height, width, _ = img.shape
    cell_height, cell_width = height // grid_size, width // grid_size

    for i in result['detections']:
        if i['class'][0] == 0.0:
            x1, y1, x2, y2 = i['coordinates']
            x_c = (x1 + x2) // 2
            y_c = (y1 + y2) // 2

            x_min = max(0, x_c - 20)
            x_max = min(img.shape[1], x_c + 35)
            y_min = int(y_c) - 30
            y_max = int(y_c) + 30
            crop = img[int(y_min):int(y_max), int(x_min):int(x_max)]

            if y_max > img.shape[0]:
                y_max = img.shape[0]

            hsv = cv2.cvtColor(crop, cv2.COLOR_BGR2HSV)
            mask = cv2.inRange(hsv, lower_purple, upper_purple)

            percentage = cv2.countNonZero(mask) / (crop.shape[0] * crop.shape[1])
            print(percentage)

            if percentage > 0.45:
                print("threshold broken")
                cv2.rectangle(img, (int(x1), int(y1)), (int(x2), int(y2)), (0, 255, 0), 2)
                grid_x = int(x_c // cell_width)
                grid_y = int(y_c // cell_height)
                print("grid is ", grid_x, grid_y)
                purple_detected_grid[grid_y, grid_x] = True
    print("image result purple grid",purple_detected_grid)
    
    return img

def process_site(site_details, model_config, dashboard_config , site_timeout, video_file):
    grid_size = 10
    purple_detected_grid = np.zeros((grid_size, grid_size), dtype=bool)
    alert_time = int(time.time())

    while True:
        day_name = datetime.datetime.today().strftime('%A')
        for camera_name, camera_detail in site_details.items():
            camera_detail = camera_detail[0]
            intrusion_site = setup_logger(site_name=camera_name)
            intrusion_site.info(f"{datetime.datetime.now()} {camera_name} {camera_detail['ip']} {camera_detail['channel']} Frame Capture Try")
            operational_days = camera_detail['operational_days']
            timings = camera_detail['timings']
            intrusion_site.info(f"{datetime.datetime.now()} {operational_days} {day_name} {timings}  {datetime.datetime.now()} ")
            if day_name not in operational_days:
                continue
            day_index = operational_days.index(day_name)

            operational_timing = [timings[day_index]]

            if (day_name in operational_days) and is_current_time_between_intervals(operational_timing):
                # url = make_url(details=camera_detail)
                url = video_file

                try:
                    if camera_detail['protocol'] == 'http':
                        frame = http_frame_read(url, camera_detail['username'], camera_detail['password'])
                    elif camera_detail['protocol'] == 'file':
                        frame = cv2.VideoCapture(camera_detail['path'])
                except Exception as e:
                    intrusion_site.error(f"Error Getting frame : {camera_name} {camera_detail}")
                    exc_info = (type(e), e, e.__traceback__)
                    intrusion_site.error(e, exc_info=exc_info)
                    continue

                if frame is None:
                    intrusion_site.error(f"Getting None frame : {url} {camera_name} {camera_detail}")
                    continue

                name = f"{camera_name}_{camera_detail['ip']}_{camera_detail['channel']}.jpg"
                result = intrusion_model_api(model_config["intrusion_detect_endpoint"], frame, intrusion_site)
                if result is not None:
                    heatmap = result_process(result, frame, purple_detected_grid)
                    cv2.imshow("Heatmap", heatmap)
                    cv2.waitKey(1)

            if int(time.time()) - alert_time >= 3600:
                if not np.all(purple_detected_grid):
                    print("Some areas have not been visited")
                purple_detected_grid.fill(False)
                alert_time = int(time.time())

        intrusion_site.info(f"{datetime.datetime.now()} {camera_name} Time to sleep for {site_timeout} seconds")
        time.sleep(site_timeout)
        intrusion_site.info(f"{datetime.datetime.now()} {camera_name} Thread Woke Up")

def main():
    logger_main = setup_logger("main_thread")
    camera_configs = [config for config in os.listdir(f"configs/") if config.startswith("camera")]
    threads = []
    for camera_config in camera_configs:
        try:
            camera_config = load_config(f'configs/{camera_config}')
            model_config = load_config('configs/model_config.yaml')
            dashboard_config = load_config('configs/dashboard_config.yaml')
        except FileNotFoundError as e:
            logger_main.error(f'Missing configuration file: {e.filename}')
            exc_info = (type(e), e, e.traceback)
            logger_main.error(e, exc_info=exc_info)
            continue
        except yaml.YAMLError as e:
            logger_main.error(f'Error in configuration file: {e}')
            exc_info = (type(e), e, e.traceback)
            logger_main.error(e, exc_info=exc_info)
            continue
        except Exception as e:
            exc_info = (type(e), e, e.traceback)
            logger_main.error(e, exc_info=exc_info)

        site_details = camera_config['cameras']
        site_timeout = camera_config['site_timeout']
        video_file = '/home/agrex/Videos/vlc-record-2024-04-09-12h57m17s-cult.mp4'  # Replace with your video file path

        site_thread = Thread(target=process_site, args=(site_details, model_config, dashboard_config, site_timeout, video_file))
        threads.append(site_thread)
        site_thread.start()

if __name__ == "__main__":
   main()