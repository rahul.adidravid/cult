import os
import requests
from datetime import datetime
from huey import SqliteHuey
from helpers import load_config
import logging
import cv2
from io import BytesIO
from logging.handlers import RotatingFileHandler

# Constants
CONFIG_FILE = "configs/dashboard_config.yaml"
LOG_DIR = os.path.join(os.getcwd(), "logs")
IMAGE_DIR = os.path.join(os.getcwd(), "images")

# Load configuration
config = load_config(CONFIG_FILE)
POST_EVENTS_URL = config['backend_events_url']
POST_ALERTS_URL = config['backend_alerts_url']

# Create huey instance
huey = SqliteHuey()

def setup_logger():
    # Ensure log directory exists
    os.makedirs(LOG_DIR, exist_ok=True)
    os.makedirs(IMAGE_DIR, exist_ok=True)
    log_filename = f"logs/huey.log"
    logger = logging.getLogger(log_filename)
    logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler(log_filename)
    fh.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt="%Y-%m-%d %H:%M:%S")
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    return logger

def setup_logger(max_size_mb=5, backup_count=5):
    os.makedirs(LOG_DIR, exist_ok=True)
    os.makedirs(IMAGE_DIR, exist_ok=True)
    log_filename = f"logs/huey.log"
    logger = logging.getLogger(log_filename)
    if not logger.handlers:
        logger.setLevel(logging.DEBUG)
        # Create a rotating file handler
        fh = RotatingFileHandler(log_filename, maxBytes=max_size_mb * 1024 * 1024, backupCount=backup_count)
        fh.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt="%Y-%m-%d %H:%M:%S")
        fh.setFormatter(formatter)
        logger.addHandler(fh)
    return logger


def create_headers(include_auth=False):
    '''Create headers for requests.'''
    headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }

    if include_auth:
        headers['Authorization'] = 'Token ' + config['token']
    return headers


@huey.task()
def send_data_to_server(image, msg, noti_type, recorded_at, CAMERA_ID, Facility_ID, camera_name):
    logger_huey = setup_logger()
    '''Send data to server.'''
    hour_minute_string = datetime.now().strftime("%H:%M:%S")
    try:

        '''encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 80]
        _, img_encoded = cv2.imencode('.jpg', image, encode_param)
        image_file = BytesIO(img_encoded)'''

        image_path = f"images/{camera_name}"
        image_filename = os.path.basename(image_path)
        image = cv2.resize(image,(1280,720))
        cv2.imwrite(image_path, image)
        multipart_form_data = {
            'event_image': (image_filename, open(image_path, 'rb')),
            'event_recieved_at':('',recorded_at),
            'camera':('', CAMERA_ID),
            'facility': ('',Facility_ID),
            'event_message':('', f'{msg} at {hour_minute_string}'),
            'event_type': ('', noti_type),
        }

        response = requests.post(POST_EVENTS_URL, files=multipart_form_data)
        if 200 <= response.status_code < 300:
            logger_huey.info(f"Alert sent! {recorded_at} {camera_name} {msg}")
        else:
            logger_huey.error("Failed to send alert.Time: %s, Camera Name: %s, Status code: %s, Response: %s" % (recorded_at,camera_name, response.status_code, response.text))

        return response.status_code
    except FileNotFoundError:
        logger_huey.error("Image file not found: %s, Time: %s, Camera Name: %s" % (image_filename, recorded_at, camera_name))
    except Exception as e:
        logger_huey.error("An error occurred while sending data to server: %s, Time: %s, Camera Name: %s" % str(e,recorded_at, camera_name))

@huey.task()
def sendAlerts(image, msg, noti_type, recorded_at, CAMERA_ID, Facility_ID, camera_name):
    logger_huey = setup_logger()
    '''Send data to server.'''
    hour_minute_string = datetime.now().strftime("%H:%M:%S")
    try:

        '''encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 80]
        _, img_encoded = cv2.imencode('.jpg', image, encode_param)
        image_file = BytesIO(img_encoded)'''

        image_path = f"images/{camera_name}"
        image_filename = os.path.basename(image_path)
        image = cv2.resize(image,(1280,720))
        cv2.imwrite(image_path, image)
        multipart_form_data = {
            'alert_image': (image_filename, open(image_path, 'rb')),
            'alert_recieved_at':('',recorded_at),
            'camera':('', CAMERA_ID),
            'facility': ('',Facility_ID),
            'alert_message':('', f'{msg} at {hour_minute_string}'),
            'alert_type': ('', noti_type),
        }

        response = requests.post(POST_ALERTS_URL, files=multipart_form_data)

        if 200 <= response.status_code < 300:
            logger_huey.info(f"Alert sent! {recorded_at} {camera_name} {msg}")
        else:
            logger_huey.error("Failed to send alert.Time: %s, Camera Name: %s, Status code: %s, Response: %s" % (recorded_at,camera_name, response.status_code, response.text))

        return response.status_code
    except FileNotFoundError:
        logger_huey.error("Image file not found: %s, Time: %s, Camera Name: %s" % (image_filename, recorded_at, camera_name))
    except Exception as e:
        logger_huey.error("An error occurred while sending data to server: %s, Time: %s, Camera Name: %s" % str(e,recorded_at, camera_name))