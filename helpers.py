import yaml
import os
import requests
# Load environment variables from .env file
import cv2
from requests.auth import HTTPDigestAuth
from imutils.video import VideoStream
import numpy as np
import time
import datetime
import logging
#import tritonclient.http as httpclient
from dotenv import load_dotenv
from logging.handlers import RotatingFileHandler



def load_config(yaml_file):
    with open(yaml_file, 'r') as file:
        config = yaml.safe_load(file)
    return config

def make_url(details):
    if details['protocol'] == "http":
        url = make_http_url(details)
    elif details['protocol'] == "rtsp":
        url = make_rtsp_url(details)
    return url

def make_rtsp_url(details):
    if details['vendor'] == "hikvision":
        rtsp_url = f"rtsp://{details['username']}:{details['password']}@{details['ip']}:{details['port']}/Streaming/Channels/{details['channel']}01"
    elif details['vendor'] == "cpplus" or details['vendor'] == "dahua":
        rtsp_url = f"rtsp://{details['username']}:{details['password']}@{details['ip']}:{details['port']}/cam/realmonitor?channel={details['channel']}&subtype=0"
    elif details['vendor'] == "securus":
        rtsp_url = f"rtsp://{details['ip']}:{details['port']}/user={details['username']}&password={details['password']}&channel={details['channel']}&stream=0.sdp?"
    elif details['vendor'] == "uniview":
        rtsp_url = f"rtsp://{details['username']}:{details['password']}@{details['ip']}:{details['port']}/unicast/c{details['channel']}/s0/live"
    return rtsp_url

def make_http_url(details):
    if details['vendor'] == "hikvision":
        http_url = f"http://{details['ip']}:{details['port']}/ISAPI/Streaming/channels/{details['channel']}01/picture?videoResolutionWidth=1920&videoResolutionHeight=1080"
    elif details['vendor'] == "cpplus" or details['vendor'] == "dahua":
        http_url = f"http://{details['ip']}:{details['port']}/cgi-bin/snapshot.cgi?channel={details['channel']}/picture?videoResolutionWidth=1920&videoResolutionHeight=1080"
    return http_url

def http_frame_read(url,username, password):
    try:
        resp = requests.get(url, auth=HTTPDigestAuth(username, password))
        image = np.asarray(bytearray(resp.content), dtype="uint8")
        image = cv2.imdecode(image, cv2.IMREAD_COLOR)
        return image
    except Exception as e:
        # logging.error(f"No Image on {url}")
        # print(e)
        return None

def rtsp_frame_read(url):
    cap = VideoStream(url).start()
    frame = cap.read()
    time.sleep(0.1)
    frame = cap.read()
    time.sleep(0.1)
    frame = cap.read()
    cap.stop()
    return frame

def intrusion_model_api(endpoint,img,logging):
    data = {}
    load_dotenv()
    # Access the authorization token
    try:
        bearer_token = os.getenv("GENERAL_TOKEN")
      
        encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 80]
        _, img_encoded = cv2.imencode('.jpg', img, encode_param)
        image_bytes = img_encoded.tobytes()


        files=[
        ('file',("test.jpg",image_bytes,f'image/jpg'))
        ]
        headers = {
        'Authorization': f'Bearer {bearer_token}'
        }
        response = requests.request("POST", endpoint,headers=headers, data=data, files=files)
        if 200 == response.status_code:
            result = response.json()
            return result
    except Exception as e:
        logging.error(f"Getting error from {endpoint} Function: {e}")
        exc_info = (type(e), e, e.__traceback__)
        logging.error(e,exc_info=exc_info)
        return None


def is_current_time_between_intervals(time_intervals):
    current_time = datetime.datetime.now().time()
    for interval in time_intervals:
        start_str, end_str = interval.split(',')
        start_time = datetime.datetime.strptime(start_str.strip(), '%H:%M').time()
        end_time = datetime.datetime.strptime(end_str.strip(), '%H:%M').time()
        
        if end_time < start_time:
            if current_time >= start_time or current_time <= end_time:
                return True
        elif start_time <= current_time <= end_time:
            return True
    return False

def setup_logger(site_name, max_size_mb=10, backup_count=5):
    log_dir = os.path.join(os.getcwd(), "logs")
    image_dir = os.path.join(os.getcwd(), "images")

    os.makedirs(log_dir, exist_ok=True)
    os.makedirs(image_dir, exist_ok=True)

    if site_name is None:
        log_filename = os.path.join(log_dir, "temp.log")
    else:
        log_filename = os.path.join(log_dir, f"{site_name}.log")

    logger = logging.getLogger(log_filename)
    if not logger.handlers:
        logger.setLevel(logging.DEBUG)

        # Create a rotating file handler
        fh = RotatingFileHandler(log_filename, maxBytes=max_size_mb * 1024 * 1024, backupCount=backup_count)
        fh.setLevel(logging.DEBUG)

        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt="%Y-%m-%d %H:%M:%S")
        fh.setFormatter(formatter)

        logger.addHandler(fh)

    return logger
    

class intrusion_businness_logic:
    def __init__(self,detections,model_config,logging):
        # class labels for idx reference 
        self.det = detections["detections"]
        self.person_idx = model_config['person_idx']
        self.bbox_lst = []
        self.class_lst = []
        self.conf_thres = model_config["confidence"]
        self.logging = logging
        self.parse_detections_1()


    def parse_detections_1(self):
        for r in self.det:
            confidence = r["confidence"][0]
            if confidence > self.conf_thres:
                self.bbox_lst.append(r["coordinates"])
                self.class_lst.append(r["class"][0])
                # Map class_lst to int
                self.class_lst = list(map(int, self.class_lst))


    def person_present(self):
        if any(item in self.class_lst for item in self.person_idx):
            return True
        return False


    def people_count(self):
        if self.person_present():
            self.people_cnt = self.class_lst.count(0)        
            return self.people_cnt
        else:
            return 0
        
    def find_centroid(self,bbox):
        x1, y1, x2, y2 = bbox
        centroid_x = (x1 + x2) / 2
        centroid_y = (y1 + y2) / 2
        return centroid_x, centroid_y

    def person_in_roi(self, roi):
        for bboxes,class_id  in zip(self.bbox_lst,self.class_lst):
            if class_id == 0:
                centroid_x, centroid_y = self.find_centroid(bboxes)
                # Convert ROI coordinates to integers
                x1, y1, x2, y2 = map(int, roi)
                if x1 < centroid_x < x2 and y1 < centroid_y < y2:
                    return True
        return False
    
    def crowd_in_roi(self, roi):
        self.people_cnt = 0
        for bboxes,class_id  in zip(self.bbox_lst,self.class_lst):
            if class_id == 0:
                centroid_x, centroid_y = self.find_centroid(bboxes)
                # Convert ROI coordinates to integers
                x1, y1, x2, y2 = map(int, roi)
                if x1 < centroid_x < x2 and y1 < centroid_y < y2:
                    self.people_cnt+=1
        return self.people_cnt
    


if    __name__ == '__main__':
    img_pth = '/media/agrex/Data/ash/jPolygon/vlcsnap-2023-09-05-16h45m37s713.png'
    img = cv2.imread(img_pth)
    logging = setup_logger("ash")
    result = intrusion_model_api("https://dl.agrexai.com/api/v1/detect",img,logging)
    model_config = load_config("configs/model_config.yaml")
    roi = [192,208,1728,928]
    if result is not None:
        intrusion = intrusion_businness_logic(result,model_config,logging)
        print(intrusion.person_in_roi(roi))
        print(intrusion.person_present())
        print(intrusion.people_count())
    # print(logistics.supervisor_present(img))
    # logistics.loading_unloading()
    # print(logistics.box_on_shoulder())
    

    


