import yaml
import os
def add_cameras(yaml_data, site, ip, username, password, vendor, protocol, port, channel, facility_id, camera_id, roi, operational_days, timings,people_count):
    camera = {
        "camera_name": f"{site}_{channel}",
        "facility_name": site,
        "ip": ip,
        "username": username,
        "password": password,
        "vendor": vendor,
        "channel": str(channel),
        "protocol": protocol,
        "port": port,
        "camera_id": camera_id,
        "facility_id": facility_id,
        "roi": roi,
        "timeout_sec": 10,  # Changed to 10 seconds as per your requirement
        "timings": timings,
        "operational_days": operational_days,
        "people_count": people_count
    }
    if site in yaml_data['cameras']:
        yaml_data['cameras'][site].append(camera)
    else:
        yaml_data['cameras'][site] = [camera]

yaml_data = {'cameras': {}}

while True:
    # Gather user input
    site = input("Enter site: ")
    ip = input("Enter IP: ")
    username = input("Enter username: ")
    password = input("Enter password: ")
    vendor = input("Enter vendor(hikvision/cpplus/dahua/securus/uniview): ")
    protocol = input("Enter protocol (rtsp/http): ")
    port = int(input("Enter port: "))
    channel = int(input("Enter Channel: "))
    facility_id = input("Enter facility ID: ")
    camera_id = input("Enter camera ID: ")
    roi = roi = input("Enter ROI[None,[x1,y1,x2,y2]]: ")
    people_count =  input("Enter Crowd Number: ")
    operational_days = []
    timings = []
    while True:
        day = input("Enter Day Name(E.g. Monday): ")
        start_time = input("Enter Start Time (E.g. 10:30): ")
        end_time = input("Enter End Time (E.g. 10:30): ")
        timing = f"{start_time}, {end_time}"
        timings.append(timing)
        operational_days.append(day)
        another_day = input("Do You Want to add another day (y/n): ")
        if another_day == "y":
            continue
        elif another_day == "n":
            break
    print(operational_days, timings)
    add_cameras(yaml_data, site, ip, username, password, vendor, protocol, port, channel, facility_id, camera_id, roi, operational_days, timings,people_count)
    
    another_entry = input("Do you want to add another entry (y/n): ")
    if another_entry != "y":
        site_timeout = int(input("Enter Camera Sleep Time: "))
        break

# Add site_timeout to yaml_data
yaml_data['site_timeout'] = site_timeout
config_name = input("Enter Name For Config: ")
os.makedirs("configs", exist_ok=True)
# Write to YAML file
with open(f"configs/camera_{config_name}.yaml", "w") as file:
    yaml.safe_dump(yaml_data, file, default_flow_style=False)
