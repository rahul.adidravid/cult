import os
import time
import yaml
import datetime
from threading import Thread
from helpers import *
from huey_worker import  sendAlerts
from shapely.geometry import Polygon, Point

lower_purple = np.array([116, 78, 130])
upper_purple = np.array([120, 113, 204])

def result_process(result,img,roi_dict,roi_status_list):
    for i in result['detections']:
        if i['class'][0] == 0.0:
            x1, y1, x2, y2=i['coordinates']
            x_c=(x1+x2) //2
            y_c=(y1+y2)//2
            # draw_boxes(img,x1, y1, x2, y2)
            
            x_min = max(0, x_c - 20)
            x_max = min(img.shape[1], x_c + 35)
            y_min = int(y_c) - 30
            y_max = int(y_c) + 30
            crop = img[int(y_min):int(y_max), int(x_min):int(x_max)]

            if y_max > img.shape[0]:
                y_max=img.shape[0]

            hsv = cv2.cvtColor(crop, cv2.COLOR_BGR2HSV)
            mask = cv2.inRange(hsv, lower_purple, upper_purple)
            
            percenrtage=cv2.countNonZero(mask) / (crop.shape[0] * crop.shape[1])
            print(percenrtage)
            # cv2.imwrite("mask/"+"cp"+str(percenrtage)+".jpg",crop)
            # cv2.imwrite("mask/"+str(percenrtage)+".jpg",mask)
            # print(mask)
            
            if percenrtage > 0.45:
                print("threshold broken")
                # Determine the grid block coordinates
                cv2.rectangle(img, (int(x1), int(y1)), (int(x2), int(y2)), (0, 255, 0), 2)
                # grid_x = int(x_c // cell_width)
                # grid_y = int(y_c // cell_height)
                # print("grid is ",grid_x,grid_y)
                # purple_detected_grid[grid_y, grid_x] = True
                center = Point(x_c, y_c)
                cv2.imwrite("images/"+str(int(time.time()))+".jpg",img)
                for i,j in roi_dict.items():
                    if j.contains(center):
                        roi_status_list[i]=1
                        print(roi_status_list)

    return img

def process_site(site_details,model_config, dashboard_config,site_timeout):
    grid_size = 4
    purple_detected_grid = np.zeros((grid_size, grid_size), dtype=bool)

    roi_status_list=[]
    roi_ls=[[(0,0),(640,0),(640,540),(0,540)],[(640,0),(1280,0),(1280,540),(640,540)],[(1280,0),(1920,0),(1920,540),(1280,540)],[(0,540),(640,540),(640,1080),(0,1080)],[(640,540),(1280,540),(1280,1080),(640,1080)],[(1280,540),(1920,540),(1920,1080),(1280,1080)]]
    roi_dict={}
    for i in range(len(roi_ls)):
        roi_dict[i]=Polygon(roi_ls[i])
        roi_status_list.append(0)
    alert_time=int(time.time())
    while True:
        day_name = datetime.datetime.today().strftime('%A')
        for camera_name,camera_detail in site_details.items():
                camera_detail = camera_detail[0]
                intrusion_site = setup_logger(site_name=camera_name)
                intrusion_site.info(f"{datetime.datetime.now()} {camera_name} {camera_detail['ip']} {camera_detail['channel']} Frame Capture Try")
                operational_days = camera_detail['operational_days']
                timings = camera_detail['timings']
                intrusion_site.info(f"{datetime.datetime.now()} {operational_days} {day_name} {timings}  {datetime.datetime.now()} ")
                if day_name not in operational_days:
                    continue
                day_index = operational_days.index(day_name)

                operational_timing = [timings[day_index]]

                if (day_name in operational_days) and is_current_time_between_intervals(operational_timing):
                    url = make_url(details=camera_detail)

                    try:
                        if camera_detail['protocol'] == 'http':
                            frame = http_frame_read(url, camera_detail['username'], camera_detail['password'])
                        elif camera_detail['protocol'] == 'rtsp':
                            url="rtsp://localhost:8554/mystream"
                            frame = rtsp_frame_read(url)
                    except Exception as e:
                        intrusion_site.error(f"Error Getting frame : {camera_name} {camera_detail}")
                        exc_info = (type(e), e, e.__traceback__)
                        intrusion_site.error(e,exc_info=exc_info)
                        continue

                    if frame is None:
                        intrusion_site.error(f"Getting None frame : {url} {camera_name} {camera_detail}")
                        continue
                    height, width, _ = frame.shape
                    cell_height, cell_width = height // grid_size, width // grid_size
                    name =  f"{camera_name}_{camera_detail['ip']}_{camera_detail['channel']}.jpg"
                    result = intrusion_model_api(model_config["intrusion_detect_endpoint"],frame,intrusion_site)
                    if result is not None:
                        img2=result_process(result,frame,roi_dict,roi_status_list)
                    

                    if int(time.time())- alert_time >= 3600:
                        for i in range(len(roi_status_list)):
                            if roi_status_list[i]==0:
                                print(f"area {i} has not been visited")
                            roi_status_list[i]=0
                        alert_time=int(time.time())
                        
                            
        intrusion_site.info(f"{datetime.datetime.now()} {camera_name} Time to sleep for {site_timeout} seconds")
        # time.sleep(site_timeout)
        intrusion_site.info(f"{datetime.datetime.now()} {camera_name} Thread Woke Up")


def main():
    logger_main = setup_logger("main_thread")
    camera_configs = [config for config in os.listdir(f"configs/") if config.startswith("camera")]
    threads = []
    for camera_config in camera_configs:
            try:
                camera_config = load_config(f'configs/{camera_config}')
                model_config = load_config('configs/model_config.yaml')
                dashboard_config = load_config('configs/dashboard_config.yaml')
            except FileNotFoundError as e:
                logger_main.error(f'Missing configuration file: {e.filename}')
                exc_info = (type(e), e, e.__traceback__)
                logger_main.error(e,exc_info=exc_info)
                continue
            except yaml.YAMLError as e:
                logger_main.error(f'Error in configuration file: {e}')
                exc_info = (type(e), e, e.__traceback__)
                logger_main.error(e,exc_info=exc_info)
                continue
            except Exception as e:
                exc_info = (type(e), e, e.__traceback__)
                logger_main.error(e,exc_info=exc_info)

            site_details = camera_config['cameras']
            site_timeout = camera_config['site_timeout']

            site_thread = Thread(target=process_site,args=(site_details,model_config,dashboard_config,site_timeout))
            threads.append(site_thread)
            site_thread.start()
            

        


if __name__ == "__main__":
    main()
